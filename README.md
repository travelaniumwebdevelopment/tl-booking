Travelanium reservation system form and link coding library

**WITH NO DEPENDENCIES**

# Basics creating URL and Parameters

## Base URL

```
https://reservation.travelanium.net/propertyibe2/
```

## Parameter

| Parameter   | Type    | Description |
| ----------- | ------- | ------------|
| propertyId  | integer | **required** - Hotel ID need to check the availability (default: null) |
| onlineId    | integer | **required** - ID for tracking the source of the form 4 = hotel individual website, 5 = hotel group website (default: 4) |
| checkin     | string  | **required** - Check-in date in format YYYY-MM-DD เช่น 2020-01-01 (default: *today*) |
| checkout    | string  | **required** - Check-out date in format YYYY-MM-DD เช่น 2020-01-01 (default: *tomorrow*) |
| numofroom   | integer | **required** - Number of room (default: 1) |
| numofadult  | integer | **required** - Number of adult (default: 2) |
| numofchild  | integer | **required** - Number of child (default: 0) |
| accesscode  | string  | Promotion code (default: null) |
| checkinauto | integer | To specific auto check-in date eg. 0 = today, 1 = tomorrow, 7 = 7 days after today. If this parameter is specified checkin and checkout parameter will be ignored (default: null) |
| numofnight  | integer | To specific number of night stay from auto check-in date, work with checkinauto parameter (default: null) |
| lang        | string  | Display language (default: en), support: en / de / ru / th / zh / zh_TW / ja / ko / vi / fr |
| currency    | string  | Display currency (default: THB), support: CNY / EUR / ILS / THB / USD / VND / AUD / GBP / MMK / KHR / CAD / DKK / EGP / HKD / INR / IDR / JPY / KZT / KWD / LAK / MYR / MVR / TWD / NZD / NOK / PHP / RUB / SGD / ZAR / KRW / SEK / CHF / TRY / AED |
| pid         | string  | Specific ID to display only specific rate plan in search result page (default: null) |
| pgroup      | string  | Redirect to the property group welcome page which will be able to select property under property group (default: null) |

---

# Usage

Basic steps using Library to create URLs in different formats.

**Library setting**

```html
<html>
  <title>Booking</title>
  <body>
    ...
    ...
    <script src="path/to/tl-booking.js"></script>
  </body>
</html>
```

**Create instances for further use**

You can set your default parameters for all reservation links in your website.

```html
<script>
  var booking = new TLBooking({
    // See below section for more details of each options.
    baseURL: 'https://reservation.travelanium.net/propertyibe2/',
    params: {
      propertyId: 29,
      lang: 'th',
      currency: 'THB',
    }
  });
</script>
```

## Options

| Options  | type  | description  |
|---|---|---|
| baseURL  | string  | To change reservation base url in case the hotel has their own custom reservation URL default: `https://reservation.travelanium.net/propertyibe2/`  |
| params  | object  | To specific the necessary parameters which can be seen from the FORM PARAMETERS table  |

---

# Methods

We can use different methods after creating instances successfully.

## url ( \$params, \$baseURL )

To create reservation URL

**Example**

```html
<script>
  var booking = new TLBooking();

  // Create default url
  console.log( booking.url() );

  // Create default url with number of adult = 3 
  // and apply "special" access code
  console.log(
    booking.url({
      numofadult: 3,
      acesscode: 'special',
    })
  );

  // Create default url with custom base URL.
  console.log(
    booking.url( null, 'https://reservation.mysweetproperty.com/propertyibe2/' )
  );
</script>
```

## form ( \$selector, \$options )

Default setup to complete the form functionality. Able to insert validation steps before submitting.

**Example**

```html
<form class="booking-form">
  <input type="text" name="checkin" value="2020-06-01">
  <input type="text" name="checkout" value="2020-06-02">
  <button type="submit">Search Reservation</button>
</form>

<script src="path/to/tl-booking.js"></script>
<script>
  var booking = new TLBooking({
    params: {
      propertyId: 29,
    }
  });

  booking.form('.booking-form', {
    // See below section for more details of each options.
    target: '_blank',
    beforeSubmit: function(form) {
      console.log( 'before submit' );
    },
    afterSubmit: function(form) {
      console.log( 'after submit' );
    },
    validate: function(form) {
      return true;
    },
  });
</script>
```

| options  | type  |  description  |
|---|---|---|
| target  | string | To select the window opening location after submitting default: `'_blank'`  |
| beforeSubmit | function | To specific callback function before form submitting |
| afterSubmit | function | To specific callback function after form submitting |
| validate | function | Insert validation before form submitting. The return value should be either true or false |


## attr( \$selector, \$attribute, \$params )

ใช้สำหรับเปลี่ยนค่าของ attribute ใน selector ที่กำหนดให้เป็น reservation url

**Example**

```html
<a class="booking-link" href="##">Booking Link</a>
<a class="booking-link-ja" href="##">Booking Link for Japanese</a>

<span class="custom-data" data-custom="">Custom Data</span>

<script src="path/to/tl-booking.js"></script>
<script>
  var booking = new TLBooking({
    params: {
      propertyId: 29,
    }
  });

  // Create link with default selector
  booking.attr('.booking-link', 'href');

  // สร้าง link ให้ selector ตามค่าเริ่มต้น แล้วแทรกค่าไปที่ attribute 'data-custom'
  booking.attr('.custom-data', 'data-custom');
</script>
```


## href( \$selector, \$params )

ทำงานเหมือน method: attr แต่จะเป็นใช้สำหรับเปลี่ยนค่า href โดยเฉพาะ (shorthand for href attribute)

**Example**

```html
<a class="booking-link" href="blablabla">Booking Link by href()</a>

<script src="path/to/tl-booking.js"></script>
<script>
  var booking = new TLBooking({
    params: {
      propertyId: 29,
    }
  });

  // Create link with default selector
  booking.href('.booking-link');

  // Create link with default selector and change the default language to be (ja)
  booking.href('.booking-link', {
    lang: 'ja',
  });
</script>
```


## setBaseURL( \$url )

Use this function to update bases URL after creating instances.

**Example**

```html
<script>
  var booking = new TLBooking({
    baseURL: 'https://reservation.travelanium.net/propertyibe2/',
    params: {
      propertyId: 29,
    },
  });

  booking.setBaseURL( 'https://reservation.mysweetproperty.com/propertyibe2/' );
</script>
```


## setParam( \$name, \$value )

Use this function to create and update parameters after creating instances.

**Example**

```html
<script>
  var booking = new TLBooking({
    params: {
      propertyId: 29,
      lang: 'th',
      currency: 'THB',
    }
  });

  booking.setParam('lang', 'en');
  booking.setParam('currency', 'USD');
  booking.setParam('accesscode', 'test');
</script>
```


## getParam( \$name )

Get the parameters setting in the present instance.

**Example**

```html
<script>
  var booking = new TLBooking({
    params: {
      propertyId: 29,
    }
  });

  alert( booking.getParam('propertyId') );
</script>
```