var {task, src, dest, watch, parallel, series} = require('gulp');
var webpack = require('webpack-stream');
var plumber = require('gulp-plumber');
var zip = require('gulp-zip');
var package = require('./package.json')
var browser = require('browser-sync').create();

const MODE = process.env.NODE_ENV || 'development';
var isProduction = (MODE === 'production');

task('script', () => {
  return src('./src/index.js')
    .pipe(plumber())
    .pipe(webpack({
      mode: MODE,
      devtool: 'source-map',
      module: {
        rules: [
          {
            test: /.js$/,
            loader: 'babel-loader',
            options: {
              'presets': ['@babel/preset-env'],
            },
          },
        ],
      },
      output: {
        filename: 'tl-booking.js',
        library: 'TLBooking',
        libraryExport: 'default',
        libraryTarget: 'umd',
      },
      optimization: {
        minimize: false,
      }
    }))
    .pipe(dest('./build'))
});

task('script:min', () => {
  return src('./src/index.js')
    .pipe(plumber())
    .pipe(webpack({
      mode: MODE,
      devtool: 'source-map',
      module: {
        rules: [
          {
            test: /.js$/,
            loader: 'babel-loader',
            options: {
              'presets': ['@babel/preset-env'],
            },
          },
        ],
      },
      output: {
        filename: 'tl-booking.min.js',
        library: 'TLBooking',
        libraryExport: 'default',
        libraryTarget: 'umd',
      },
      optimization: {
        minimize: true,
      }
    }))
    .pipe(dest('./build'))
});

task('server', () => {
  return browser.init({
    open: false,
    port: 3005,
    server: {
      baseDir: './test',
    },
    serveStatic: ['./build'],
  });
});

var reload = done => {
  browser.reload();
  done();
}

task('watch', () => {
  watch([
    './src/**/*.js'
  ], series('script'));

  watch([
    './test/**/*.html',
    './build/**/*.js',
  ], series(reload));
});

task('release', done => {
  src('./build/*')
    .pipe(dest(`./release/${package.version}/`))
    .pipe(zip(`tl-booking-v${package.version}.zip`))
    .pipe(dest(`./release/${package.version}/`))
  
  done();
});

task('default', parallel('release'));
task('build', parallel('script', 'script:min'));
task('serve', series('build', parallel('watch', 'server')));